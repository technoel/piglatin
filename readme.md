Installation:

- Change to a directory immediately above the directory containing the repository contents.

- Install vagrant, virtual box (and extensions)

- run 'vagrant box add laravel/homestead'

- add piglatin.app ip 33.33.33.60 to your hosts file

    ( /etc/hosts  or  C:\Windows\System32\drivers\etc\hosts )


- Change directory to piglatin (repository clone location).
 
- run bash ./init.sh

- run vagrant up

- open a web browser to url: http://piglatin.app