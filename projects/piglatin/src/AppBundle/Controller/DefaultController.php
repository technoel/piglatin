<?php

namespace AppBundle\Controller;

use AppBundle\Form\TranslateType;
use AppBundle\PigLatin\Translatable\Language\LanguageInterface;
use AppBundle\PigLatin\Translatable\Language\LanguageRepository;
use AppBundle\PigLatin\Translatable\Text;
use AppBundle\PigLatin\Translatable\TextFactory;
use AppBundle\PigLatin\Translator\PigLatin;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DefaultController
 * @package AppBundle\Controller
 *
 * This is the default controller for the web application.
 */
class DefaultController extends Controller
{
    /**
     * This is the default route. It handles the rendering of the translate form and submission.
     *
     * It is the index page for piglatin.app
     *
     * @Route("/", name="homepage")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $data = $this->getTextFactory()->create('', $this->getLanguageRepository()->getLanguageByLocale('en-GB'));

        $translateForm = $this->createForm(TranslateType::class, $data,[
            'action' => $this->generateUrl('homepage'),
            'method' => 'POST',
        ]);

        $translateForm->handleRequest($request);

        if ($translateForm->isSubmitted() && $translateForm->isValid()) {
            /** @var Text $text */
            $data = $translateForm->getData();
            $translator = $this->getPigLatinTranslator();

            $destinationLanguage = $this->getDestinationLanguage($data->getLanguage());

            $data = $translator->translate($data, $destinationLanguage);
        }

        // render the index twig template
        return $this->render('AppBundle:Default:index.html.twig', [
            'translate_form'=>$translateForm->createView(),
            'translated_text'=>$data,
        ]);
    }

    /**
     * Convenience function to calculate a destination language depending on the locale of the submitted text data.
     *
     * @param LanguageInterface $inputLanguage
     * @return LanguageInterface|null
     */
    private function getDestinationLanguage(LanguageInterface $inputLanguage)
    {
        switch($inputLanguage->getLocale())
        {
            case 'en-GB';
                    return $this->getLanguageRepository()->getLanguageByLocale('pl-PL');
                break;
            default:
                return $this->getLanguageRepository()->getLanguageByLocale('en-GB');
        }
    }

    /**
     * @return TextFactory
     */
    private function getTextFactory() : TextFactory
    {
        return $this->get('factory.text');
    }

    /**
     * @return LanguageRepository
     */
    private function getLanguageRepository() : LanguageRepository
    {
        return $this->get('repository.language');
    }

    /**
     * @return PigLatin
     */
    private function getPigLatinTranslator() : PigLatin
    {
        return $this->get('translator.pig.latin');
    }
}
