<?php
/**
 * Created by PhpStorm.
 * User: cannibal
 * Date: 03/02/2017
 * Time: 12:18
 */

namespace AppBundle\PigLatin\Tokeniser;

/**
 * Class TokenisedTextFactory
 * @package AppBundle\PigLatin\Tokeniser
 *
 * This class is the factory for TokenisedText objects
 */
class TokenisedTextFactory
{
    public function create() : TokenisedText
    {
        return new TokenisedText();
    }
}