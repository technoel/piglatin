<?php
/**
 * Created by PhpStorm.
 * User: cannibal
 * Date: 02/02/2017
 * Time: 14:54
 */

namespace AppBundle\PigLatin\Tokeniser;

use AppBundle\PigLatin\Translatable\TextFactory;
use AppBundle\PigLatin\Translatable\TranslatableInterface;

/**
 * Class WordBoundary
 * @package AppBundle\PigLatin\Tokeniser
 *
 * This class tokenises a TranslatableText in to individual components based on word boundaries in RegExp.
 */
class WordBoundary implements TokeniserInterface
{
    private $tokenisedTextfactory;
    private $textFactory;

    public function __construct(TextFactory $textFactory, TokenisedTextFactory $tokenisedTextFactory)
    {
        $this->tokenisedTextfactory = $tokenisedTextFactory;
        $this->textFactory = $textFactory;
    }

    public function tokenise(TranslatableInterface $text) : TokenisableInterface
    {
        $items = preg_match_all('/(\b)?([\w\'\-]+|[\W\s\n\r\s]+)(\b)?/i', $text->getText(), $matches);

        $out = $this->getTokenisedTextFactory()->create();

        if(empty($text)){
            return $out;
        }

        $out->setTokens($matches[2]);
        $out->setLanguage($text->getLanguage());

        return $out;
    }

    public function combine(TokenisableInterface $parsable) : TranslatableInterface
    {
        $text = '';

        foreach($parsable as $key => $item){
            $text .= $item;
        }

        $out = $this->getTextFactory()->create($text, $parsable->getLanguage());

        return $out;
    }

    /**
     * @return TokenisedTextFactory
     */
    protected function getTokenisedTextFactory()
    {
        return $this->tokenisedTextfactory;
    }

    /**
     * @return TextFactory
     */
    protected function getTextFactory()
    {
        return $this->textFactory;
    }
}