<?php
/**
 * Created by PhpStorm.
 * User: cannibal
 * Date: 01/02/2017
 * Time: 18:02
 */

namespace AppBundle\PigLatin\Tokeniser;


use AppBundle\PigLatin\Translatable\TranslatableInterface;

/**
 * Interface TokeniserInterface
 * @package AppBundle\PigLatin\Tokeniser
 *
 * A class that implements the TokeniserInterface can break a piece of TranslatableText down in to individual components that can be parsed by a Translator.
 */
interface TokeniserInterface
{
    /**
     * This function tokenises a piece of Translatable Text in to atomic components that can be parsed.
     *
     * @param TranslatableInterface $text
     * @return TokenisableInterface
     */
    public function tokenise(TranslatableInterface $text) : TokenisableInterface;

    /**
     * This method combines Tokenised Text in to a single piece of Translatable Text.
     *
     * @param TokenisableInterface $tokenised
     * @return TranslatableInterface
     */
    public function combine(TokenisableInterface $tokenised) : TranslatableInterface;
}