<?php
/**
 * Created by PhpStorm.
 * User: cannibal
 * Date: 02/02/2017
 * Time: 14:56
 */

namespace AppBundle\PigLatin\Tokeniser;

use AppBundle\PigLatin\Translatable\Language\LanguageInterface;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class TokenisedText
 * @package AppBundle\PigLatin\Tokeniser
 *
 * This class represents a piece of text in a particular locale that has been tokenised in to component parts (words and punctuation).
 */
class TokenisedText implements TokenisableInterface
{
    private $tokens;
    private $language;

    public function __construct()
    {
        $this->tokens = new ArrayCollection();
        $this->language = null;
    }

    /**
     * @inheritdoc
     */
    public function toArray() : array
    {
        return $this->tokens->toArray();
    }

    /**
     * @inheritdoc
     */
    public function setTokens(array $tokens)
    {
        $this->tokens->clear();
        $this->tokens = new ArrayCollection($tokens);
    }

    /**
     * @inheritdoc
     */
    public function setToken($key, $token)
    {
        $this->tokens->set($key, $token);
    }

    /**
     * @inheritdoc
     */
    public function getLanguage() : LanguageInterface
    {
        return $this->language;
    }

    /**
     * @inheritdoc
     */
    public function setLanguage(LanguageInterface $language)
    {
        $this->language = $language;
    }

    //Iterator methods
    /**
     * Return the current element
     * @link http://php.net/manual/en/iterator.current.php
     * @return mixed Can return any type.
     * @since 5.0.0
     */
    public function current()
    {
        return $this->tokens->current();
    }

    /**
     * Move forward to next element
     * @link http://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function next()
    {
        $this->tokens->next();
    }

    /**
     * Return the key of the current element
     * @link http://php.net/manual/en/iterator.key.php
     * @return mixed scalar on success, or null on failure.
     * @since 5.0.0
     */
    public function key()
    {
        return $this->tokens->key();
    }

    /**
     * Checks if current position is valid
     * @link http://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     * @since 5.0.0
     */
    public function valid()
    {
        return !empty($this->tokens->current());
    }

    /**
     * Rewind the Iterator to the first element
     * @link http://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function rewind()
    {
        $this->tokens->first();
    }


}