<?php
/**
 * Created by PhpStorm.
 * User: cannibal
 * Date: 02/02/2017
 * Time: 14:59
 */

namespace AppBundle\PigLatin\Tokeniser;

use AppBundle\PigLatin\Translatable\Language\LanguageInterface;

/**
 * Interface TokenisableInterface
 * @package AppBundle\PigLatin\Tokeniser
 *
 * Classes that implement this interface contain a string that has been tokenised in to individual components that can be used for further processing.
 */
interface TokenisableInterface extends \Iterator
{
    /**
     * @param array $tokens
     * @return mixed
     *
     * This method is used to set all tokens.
     */
    public function setTokens(array $tokens);

    /**
     * @return LanguageInterface
     *
     * This method return the Language the tokenised text is written in.
     */
    public function getLanguage() : LanguageInterface;

    /**
     * @param LanguageInterface $language
     * @return mixed
     *
     * This method allows the Language the tokenised text is written in to be updated.
     */
    public function setLanguage(LanguageInterface $language);

    /**
     * @param $key
     * @param $value
     * @return mixed
     *
     * This method is used to update the value of a specific token.
     */
    public function setToken($key, $value);

    /**
     * @return array
     *
     * Returns all the tokens in an array
     */
    public function toArray() : array;
}