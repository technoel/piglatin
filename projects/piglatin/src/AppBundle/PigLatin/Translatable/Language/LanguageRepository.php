<?php
/**
 * Created by PhpStorm.
 * User: cannibal
 * Date: 03/02/2017
 * Time: 14:07
 */

namespace AppBundle\PigLatin\Translatable\Language;


use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class LanguageRepository
 * @package AppBundle\PigLatin\Translatable\Language
 *
 * This class creates Language objects.
 *
 * For simplicity the constructor seeds it with some example languages, but in reality this data would come from a database or static file.
 */
class LanguageRepository
{
    private $languages;

    public function __construct(LanguageFactory $languageFactory)
    {
        $this->languages = new ArrayCollection();

        $this->languages->set('en-GB',$languageFactory->create('English', 'en-GB'));
        $this->languages->set('pl-PL',$languageFactory->create('Pig Latin', 'pl-PL'));
    }

    public function getSupportedLanguages()
    {
        return $this->languages->toArray();
    }

    public function getLanguageByLocale($locale) : ?LanguageInterface
    {
        return $this->languages->get($locale);
    }
}