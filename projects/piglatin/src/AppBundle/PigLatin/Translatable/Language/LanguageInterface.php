<?php
/**
 * Created by PhpStorm.
 * User: cannibal
 * Date: 01/02/2017
 * Time: 18:57
 */

namespace AppBundle\PigLatin\Translatable\Language;

/**
 * Interface LanguageInterface
 * @package AppBundle\PigLatin\Translatable\Language
 *
 * LanguageInterface defines the methods (contract) that are required for classes that use supported Languages.
 */
interface LanguageInterface
{
    public function getName() : ?string;

    public function getLocale() : ?string;

    public function equals(LanguageInterface $language) : bool;
}