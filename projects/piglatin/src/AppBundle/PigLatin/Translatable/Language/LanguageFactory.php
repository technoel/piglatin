<?php
/**
 * Created by PhpStorm.
 * User: cannibal
 * Date: 02/02/2017
 * Time: 14:14
 */

namespace AppBundle\PigLatin\Translatable\Language;

/**
 * Class LanguageFactory
 * @package AppBundle\PigLatin\Translatable\Language
 *
 * This class creates Language objects.
 */
class LanguageFactory
{
    public function create($name, $locale) : LanguageInterface
    {
        $out = new Language();

        $out->setLocale($locale);
        $out->setName($name);

        return $out;
    }
}