<?php
/**
 * Created by PhpStorm.
 * User: cannibal
 * Date: 02/02/2017
 * Time: 14:14
 */

namespace AppBundle\PigLatin\Translatable\Language;

/**
 * Class Language
 * @package AppBundle\PigLatin\Translatable\Language
 *
 * This class represents a supported Language used for translations. It could be implemented as a simple class with a few constants depending on long term requirements scope.
 */
class Language implements LanguageInterface
{
    private $locale;
    private $name;

    public function __construct()
    {
        $this->locale = null;
        $this->name = null;
    }

    /**
     * @return null
     */
    public function getLocale() : ?string
    {
        return $this->locale;
    }

    /**
     * @param null $locale
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * @return null
     */
    public function getName() : ?string
    {
        return $this->name;
    }

    /**
     * @param null $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    public function equals(LanguageInterface $language) : bool
    {
        return $this->getLocale() == $language->getLocale() && $this->getName() == $language->getName();
    }
}