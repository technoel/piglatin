<?php
/**
 * Created by PhpStorm.
 * User: cannibal
 * Date: 01/02/2017
 * Time: 19:09
 */

namespace AppBundle\PigLatin\Translatable;

use AppBundle\PigLatin\Tokeniser\TokenisableInterface;
use AppBundle\PigLatin\Translatable\Language\LanguageInterface;

/**
 * Class TextFactory
 * @package AppBundle\PigLatin\Translatable
 *
 * This class creates Text objects.
 */
class TextFactory
{
    public function create(string $text, LanguageInterface $language) : TranslatableInterface
    {
        $out = new Text();

        $out->setText($text);
        $out->setLanguage($language);

        return $out;
    }
}