<?php
/**
 * Created by PhpStorm.
 * User: cannibal
 * Date: 01/02/2017
 * Time: 19:11
 */

namespace AppBundle\PigLatin\Translatable;

use AppBundle\PigLatin\Translatable\Language\LanguageInterface;

/**
 * Class Text
 * @package AppBundle\PigLatin\Translatable
 *
 * This class represents a piece of chunk of text that is in a particular language.
 */
class Text implements TranslatableInterface
{
    private $text;
    private $language;

    public function __construct()
    {
        $this->text = null;
        $this->language = null;
    }

    /**
     * @return mixed
     */
    public function getText() : ?string
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     */
    public function setText(?string $text)
    {
        $this->text = $text;
    }

    /**
     * @return mixed
     */
    public function getLanguage() :?LanguageInterface
    {
        return $this->language;
    }

    /**
     * @param mixed $language
     */
    public function setLanguage(?LanguageInterface $language)
    {
        $this->language = $language;
    }
}