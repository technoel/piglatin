<?php
namespace AppBundle\PigLatin\Translatable;

use AppBundle\PigLatin\Translatable\Language\LanguageInterface;

/**
 * Interface TranslatableInterface
 * @package AppBundle\PigLatin\Translatable
 *
 * This interface defines the methods required by classes that are translatable.
 */
interface TranslatableInterface
{
    public function getText() : ?string;

    public function getLanguage() : ?LanguageInterface;

    public function setText(?string $text);

    public function setLanguage(?LanguageInterface $language);
}