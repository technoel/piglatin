<?php
namespace AppBundle\PigLatin\Translator;


use AppBundle\PigLatin\Translatable\Language\LanguageInterface;
use AppBundle\PigLatin\Translatable\TranslatableInterface;

/**
 * Interface TranslatorInterface
 * @package AppBundle\PigLatin\Translator
 *
 * This interface defines the methods that are required by Translator capable of transforming one piece of TranslatableText in to another.
 */
interface TranslatorInterface
{
    /**
     * @param TranslatableInterface $toTranslate
     * @param LanguageInterface $locale
     * @param array $options
     * @return TranslatableInterface|null
     *
     * This  method transforms a piece of TranslatableText in to TranslatableText in a supported Language.
     */
    public function translate(TranslatableInterface $toTranslate, LanguageInterface $locale, array $options = []) : ?TranslatableInterface;

    /**
     * @param TranslatableInterface $toTranslate
     * @return mixed
     *
     * This method returns a boolean that denotes whether a particular Translatable can be translated by this translator.
     */
    public function canTranslate(TranslatableInterface $toTranslate);
}