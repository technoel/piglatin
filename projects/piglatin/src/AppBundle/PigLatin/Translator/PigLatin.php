<?php
/**
 * Created by PhpStorm.
 * User: cannibal
 * Date: 01/02/2017
 * Time: 18:58
 */
namespace AppBundle\PigLatin\Translator;

use AppBundle\PigLatin\Tokeniser\TokenisableInterface;
use AppBundle\PigLatin\Tokeniser\TokeniserInterface;
use AppBundle\PigLatin\Translatable\Language\LanguageInterface;
use AppBundle\PigLatin\Translatable\Language\LanguageRepository;
use AppBundle\PigLatin\Translatable\TranslatableInterface;
use AppBundle\PigLatin\Translatable\TranslationException;

/**
 * Class PigLatin
 * @package AppBundle\PigLatin\Translator
 *
 * This class translates TranslatableText in English to Pig Latin and vice versa.
 */
class PigLatin implements TranslatorInterface
{
    const CONSONANTS = '[BCDFGHJKLMNPQRSTVXZWY]';
    const CONSONANT_CLUSTER = '[HU]';

    private $tokeniser;
    private $languageRepository;

    public function __construct(TokeniserInterface $tokeniser, LanguageRepository $languageRepository)
    {
        $this->tokeniser = $tokeniser;
        $this->languageRepository = $languageRepository;
    }

    /**
     * @inheritdoc
     */
    public function translate(TranslatableInterface $toTranslate, LanguageInterface $toLocale, array $options = []): ?TranslatableInterface
    {
        $tokenised = $this->getTokeniser()->tokenise($toTranslate);

        switch ($toLocale->getLocale()) {
            case 'pl-PL':
                $tokenised = $this->englishToPigLatin($tokenised, $options);
                break;
            case 'en-GB':
                $tokenised = $this->pigLatinToEnglish($tokenised);
                break;
            default:
                throw new TranslationException(sprintf('Could not translate the language %s', $toLocale->getName()));
        }

        $toTranslate = $this->getTokeniser()->combine($tokenised);

        return $toTranslate;

    }

    private function pigLatinToEnglish(TokenisableInterface $toTranslate): TokenisableInterface
    {
        $toTranslate = clone($toTranslate);
        foreach ($toTranslate as $key => $item) {
            if (preg_match('/^[\W\s\n\r\s]+$/', $item) == false) {

                //small optimisation as regular expressions are SLOW!
                if(strpos($item,"-")){
                    $item = preg_replace('/(\w+)\-('.PigLatin::CONSONANTS.'*'.PigLatin::CONSONANT_CLUSTER.'*?)ay/i', '$2$1', $item);
                }
                else{
                    $item = preg_replace('/(\w+)\'(\w)?ay/i', '$1', $item);
                }
                $toTranslate->setToken($key, $item);
            }
        }

        $toTranslate->setLanguage($this->getLanguageRepository()->getLanguageByLocale('en-GB'));

        return $toTranslate;
    }

    private function englishToPigLatin(TokenisableInterface $toTranslate, array $options): TokenisableInterface
    {
        $toTranslate = clone($toTranslate);
        $startsWithConsonant = '/^' . PigLatin::CONSONANTS . '+/i';

        foreach ($toTranslate as $key => $item) {
            if (preg_match('/^[\W\s\n\r\s]+$/', $item) == false) { //is not punctuation

                if (preg_match($startsWithConsonant, $item) == true) //starts with consonant
                {
                    $item = preg_replace('/^(' . PigLatin::CONSONANTS . '+u?)(\w+)/i', '$2-$1ay', $item);
                }
                else {
                    $dialect = '';
                    if(array_key_exists('vowel_dialect', $options) && !empty($options['vowel_dialect'])){
                        $dialect = $options['vowel_dialect'];
                    }
                    $item = $item . "'" . $dialect . 'ay';
                }
                $toTranslate->setToken($key, $item);
            }
        }

        $toTranslate->setLanguage($this->getLanguageRepository()->getLanguageByLocale('pl-PL'));

        return $toTranslate;
    }

    /**
     * @inheritdoc
     */
    public function canTranslate(TranslatableInterface $toTranslate)
    {
        $language = $toTranslate->getLanguage();

        switch ($language->getLocale()) {
            case 'pl-PL':
            case 'en-GB':
                return true;
                break;
        }

        return false;
    }

    /**
     * @return LanguageRepository
     */
    protected function getLanguageRepository(): LanguageRepository
    {
        return $this->languageRepository;
    }

    /**
     * @return TokeniserInterface
     */
    protected function getTokeniser(): TokeniserInterface
    {
        return $this->tokeniser;
    }
}