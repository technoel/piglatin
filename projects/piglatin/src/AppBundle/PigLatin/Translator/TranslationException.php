<?php
/**
 * Created by PhpStorm.
 * User: cannibal
 * Date: 03/02/2017
 * Time: 16:23
 */

namespace AppBundle\PigLatin\Translatable;

/**
 * Class TranslationException
 * @package AppBundle\PigLatin\Translatable
 *
 * This class is throw when there is an expected state during translation of text by a class implementing TranslatorInterface.
 */
class TranslationException extends \Exception
{

}