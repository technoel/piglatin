<?php
/**
 * Created by PhpStorm.
 * User: cannibal
 * Date: 02/02/2017
 * Time: 11:02
 */
namespace AppBundle\Form;

use AppBundle\PigLatin\Translatable\Language\LanguageInterface;
use AppBundle\PigLatin\Translatable\Language\LanguageRepository;
use AppBundle\PigLatin\Translatable\Text;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class TranslateType
 * @package AppBundle\Form
 *
 * This class codes for a form to allow TranslatableText to be submitted.
 */
class TranslateType extends AbstractType
{
    private $languageRepository;

    public function __construct(LanguageRepository $languageRepository)
    {
        $this->languageRepository = $languageRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('text', TextareaType::class, [
                'required' => false,
                'label'=>false,
            ])
            ->add('language', ChoiceType::class, [
                'choices'=> $this->getLanguageRepository()->getSupportedLanguages(),
                'choice_label'=>function(LanguageInterface $language, ?string $key, ?string $index){
                    return $language->getName();
                },
                'choice_value'=>function(LanguageInterface $language){
                    return $language->getLocale();
                },
                'label'=>false,
                'required'=>true,
            ])
            ->add('submit', SubmitType::class)
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Text::class,
        ));
    }

    protected function getLanguageRepository() : LanguageRepository
    {
        return $this->languageRepository;
    }
}