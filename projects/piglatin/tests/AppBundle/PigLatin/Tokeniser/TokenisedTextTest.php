<?php
/**
 * Created by PhpStorm.
 * User: cannibal
 * Date: 03/02/2017
 * Time: 12:29
 */

namespace Tests\AppBundle\PigLatin\Tokeniser;


use AppBundle\PigLatin\Tokeniser\TokenisedText;

class TokenisedTextTest extends \PHPUnit_Framework_TestCase
{
    public function testConstructor() : TokenisedText
    {
        $item = new TokenisedText();

        $this->assertEquals([], $item->toArray());

        return $item;
    }

    public function testSetToken()
    {
        $item = $this->testConstructor();

        $tokens = ['one', 'two', '.', '....'];

        $item->setTokens($tokens);
    }
}