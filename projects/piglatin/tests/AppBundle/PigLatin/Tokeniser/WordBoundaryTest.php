<?php
/**
 * Created by PhpStorm.
 * User: cannibal
 * Date: 03/02/2017
 * Time: 12:41
 */

namespace Tests\AppBundle\PigLatin\Tokeniser;


use AppBundle\PigLatin\Tokeniser\WordBoundary;
use AppBundle\PigLatin\Tokeniser\TokenisedText;
use AppBundle\PigLatin\Tokeniser\TokenisedTextFactory;
use AppBundle\PigLatin\Translatable\Language\LanguageInterface;
use AppBundle\PigLatin\Translatable\TextFactory;
use AppBundle\PigLatin\Translatable\TranslatableInterface;

class WordBoundaryTest extends \PHPUnit_Framework_TestCase
{
    public function dataProviderTestTokenise()
    {
        return [
            ['One two three!', ['One', ' ', 'two', ' ', 'three', '!']],
            ['One. two, three...', ['One', '. ', 'two', ', ', 'three', '...']]
        ];
    }

    /**
     * @dataProvider dataProviderTestTokenise
     */
    public function testTokenise($textIn, $tokens)
    {
        $language = $this->createLanguageMock();

        $text = $this->createTranslatableTextMock();
        $text->method('getText')->willReturn($textIn);
        $text->method('getLanguage')->willReturn($language);

        $textFactory = $this->createTextFactoryMock();

        $tokenisedText = $this->createTokenisedTextMock();
        $tokenisedText->expects($this->once())->method('setTokens')->with($tokens);
        $tokenisedTextFactory = $this->createTokenisedTextFactoryMock();
        $tokenisedTextFactory->expects($this->once())->method('create')->willReturn($tokenisedText);

        $item = new WordBoundary($textFactory, $tokenisedTextFactory);

        $out = $item->tokenise($text);
        $this->assertEquals($out, $tokenisedText);
    }

    /**
     * @dataProvider dataProviderTestTokenise
     */
    public function testToString($textIn, $tokens)
    {
        $translatableText = $this->createTranslatableTextMock();
        $tokenisedTextFactory = $this->createTokenisedTextFactoryMock();
        $language = $this->createLanguageMock();

        $textFactory = $this->createTextFactoryMock();
        $textFactory->expects($this->once())->method('create')->with($textIn, $language)->willReturn($translatableText);

        $data = new TokenisedText();
        $data->setTokens($tokens);
        $data->setLanguage($language);

        $item = new WordBoundary($textFactory, $tokenisedTextFactory);
        $out = $item->combine($data);

        $this->assertEquals($translatableText, $out);
    }

    public function createTokenisedTextMock()
    {
        return $this->createMock(TokenisedText::class);
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject|TokenisedTextFactory
     */
    public function createTokenisedTextFactoryMock()
    {
        return $this->createMock(TokenisedTextFactory::class);
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject|TextFactory
     */
    public function createTextFactoryMock()
    {
        return $this->createMock(TextFactory::class);
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject|TranslatableInterface
     */
    public function createTranslatableTextMock()
    {
        return $this->createMock(TranslatableInterface::class);
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject|LanguageInterface
     */
    public function createLanguageMock()
    {
        return $this->createMock(LanguageInterface::class);
    }
}