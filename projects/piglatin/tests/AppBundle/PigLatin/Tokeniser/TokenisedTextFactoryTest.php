<?php
/**
 * Created by PhpStorm.
 * User: cannibal
 * Date: 03/02/2017
 * Time: 12:25
 */

namespace Tests\AppBundle\PigLatin\Tokeniser;


use AppBundle\PigLatin\Tokeniser\TokenisedText;
use AppBundle\PigLatin\Tokeniser\TokenisedTextFactory;

class TokenisedTextFactoryTest extends \PHPUnit_Framework_TestCase
{
    public function testCreate()
    {
        $out = new TokenisedTextFactory();

        $this->assertInstanceOf(TokenisedText::class, $out->create());
    }
}