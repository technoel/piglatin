<?php
/**
 * Created by PhpStorm.
 * User: cannibal
 * Date: 03/02/2017
 * Time: 16:13
 */

namespace Tests\AppBundle\PigLatin\Translator;

use AppBundle\PigLatin\Tokeniser\WordBoundary;
use AppBundle\PigLatin\Tokeniser\TokenisedTextFactory;
use AppBundle\PigLatin\Translatable\Language\Language;
use AppBundle\PigLatin\Translatable\Language\LanguageFactory;
use AppBundle\PigLatin\Translatable\Language\LanguageRepository;
use AppBundle\PigLatin\Translator\PigLatin;
use AppBundle\PigLatin\Translatable\TextFactory;

class PigLatinTest extends \PHPUnit_Framework_TestCase
{
    public function dataProviderTranslate()
    {
        return [
            ['beast', 'east-bay'],
            ['dough', 'ough-day'],
            ['happy', 'appy-hay'],
            ['question', 'estion-quay'],
            ['star', 'ar-stay'],
            ['three', 'ee-thray'],
            ['eagle', 'eagle\'ay'],
            ["This is a longer test, that includes some punctuation.\nA new paragraph; Something else?... perhaps?!", "is-Thay is'ay a'ay onger-lay est-tay, at-thay includes'ay ome-say nctuation-puay.\nA'ay ew-nay aragraph-pay; omething-Say else'ay?... erhaps-pay?!"]
        ];
    }

    /**
     * @dataProvider dataProviderTranslate
     */
    public function testTranslate($textIn, $expectedResult)
    {
        $factory = $this->getTextFactory();
        $languageRepository = $this->getLanguageRepository();

        $tokeniser = new WordBoundary($factory, new TokenisedTextFactory());
        $translator = new PigLatin($tokeniser, $this->getLanguageRepository());

        $text = $factory->create($textIn, $languageRepository->getLanguageByLocale('en-GB'));

        $result = $translator->translate($text, $languageRepository->getLanguageByLocale('pl-PL'));

        $this->assertEquals('pl-PL', $result->getLanguage()->getLocale());
        $this->assertEquals($expectedResult, $result->getText());

    }

    /**
     * @dataProvider dataProviderTranslate
     */
    public function testReverseTranslate($expectedResult, $textIn)
    {
        $factory = $this->getTextFactory();
        $languageRepository = $this->getLanguageRepository();

        $tokeniser = new WordBoundary($factory, new TokenisedTextFactory());
        $translator = new PigLatin($tokeniser, $this->getLanguageRepository());

        $text = $factory->create($textIn, $languageRepository->getLanguageByLocale('pl-PL'));

        $result = $translator->translate($text, $languageRepository->getLanguageByLocale('en-GB'));

        $this->assertEquals('en-GB', $result->getLanguage()->getLocale());
        $this->assertEquals($expectedResult, $result->getText());
    }

    public function testTranslateWithDialect()
    {
        $this->markTestIncomplete();
    }

    public function getLanguageRepository()
    {
        return new LanguageRepository(new LanguageFactory());
    }

    public function getTextFactory()
    {
        return new TextFactory();
    }
}