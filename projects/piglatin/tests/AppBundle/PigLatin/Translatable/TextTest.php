<?php
namespace Tests\AppBundle\PigLatin\Translatable;

use AppBundle\PigLatin\Translatable\Language\LanguageInterface;
use AppBundle\PigLatin\Translatable\Text;


/**
 * Created by PhpStorm.
 * User: cannibal
 * Date: 02/02/2017
 * Time: 16:51
 */
class TextTest extends \PHPUnit_Framework_TestCase
{
    public function testConstructor() : Text
    {
        $item = new Text();
        $this->assertNull($item->getText());
        $this->assertNull($item->getLanguage());

        return $item;
    }

    public function testSetGetText()
    {
        $item = $this->testConstructor();

        $testText = 'test';

        $item->setText($testText);

        $this->assertEquals($testText, $item->getText(), 'Cannot set or get text on Text');
    }

    public function createLanguageMock() : \PHPUnit_Framework_MockObject_MockObject
    {
        return $this->createMock(LanguageInterface::class);
    }

    public function testSetGetLanguage()
    {
        $item = $this->testConstructor();

        $testLang = $this->createLanguageMock();

        $item->setLanguage($testLang);

        $this->assertEquals($testLang, $item->getLanguage());
    }
}