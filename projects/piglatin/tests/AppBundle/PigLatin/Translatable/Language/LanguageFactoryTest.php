<?php
/**
 * Created by PhpStorm.
 * User: cannibal
 * Date: 01/02/2017
 * Time: 19:14
 */

namespace Tests\AppBundle\PigLatin\Translatable\Language;


use AppBundle\PigLatin\Translatable\Language\LanguageFactory;
use AppBundle\PigLatin\Translatable\Language\LanguageInterface;

class LanguageFactoryTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @return \PHPUnit_Framework_MockObject_MockObject|LanguageInterface
     */
    public function getLanguageInterfaceMock() : \PHPUnit_Framework_MockObject_MockObject
    {
        return $this->createMock(LanguageInterface::class);
    }

    /**
     * @return LanguageFactory
     */
    public function createFactory() : LanguageFactory
    {
        return new LanguageFactory();
    }

    public function testCreate()
    {
        $factory = $this->createFactory();

        $name = 'test';
        $locale = 'testLocale';

        $language = $factory->create($name, $locale);

        $this->assertEquals($name, $language->getName());
        $this->assertEquals($locale, $language->getLocale());
    }
}