<?php
/**
 * Created by PhpStorm.
 * User: cannibal
 * Date: 01/02/2017
 * Time: 19:14
 */

namespace Tests\AppBundle\PigLatin\Translatable\Language;


use AppBundle\PigLatin\Translatable\Language\Language;

class LanguageTest extends \PHPUnit_Framework_TestCase
{
    public function testConstructor()
    {
        $out = new Language();

        $this->assertInstanceOf(Language::class, $out, 'Instance of language not created');

        $this->assertNull($out->getName());

        $this->assertNull($out->getLocale());

        return $out;
    }

    public function testSetGetName()
    {
        $item = $this->testConstructor();

        $testName = 'test';

        $item->setName($testName);

        $this->assertEquals($testName, $item->getName(), 'Cannot set or get Name on Language');
    }

    public function testSetGetLocale()
    {
        $item = $this->testConstructor();

        $testLocale = 'en-GB';

        $item->setLocale($testLocale);

        $this->assertEquals($testLocale, $item->getLocale(), 'Cannot set or get Locale on Language');

    }

    public function testEquals()
    {
        $item = new Language();
        $item->setName('one');
        $item->setLocale('one-locale');

        $item2 = new Language();
        $item2->setName('two');
        $item2->setLocale('two-locale');

        $this->assertFalse($item->equals($item2));
        $this->assertFalse($item2->equals($item));

        $item->setName('test');
        $item->setLocale('test-locale');
        $item2->setName('test');
        $item2->setLocale('test-locale');

        $this->assertTrue($item->equals($item2));
        $this->assertTrue($item2->equals($item));
    }
}