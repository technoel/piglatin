<?php
/**
 * Created by PhpStorm.
 * User: cannibal
 * Date: 02/02/2017
 * Time: 17:20
 */

namespace Tests\AppBundle\PigLatin\Translatable;


use AppBundle\PigLatin\Translatable\Language\LanguageInterface;
use AppBundle\PigLatin\Translatable\Text;
use AppBundle\PigLatin\Translatable\TextFactory;

class TextFactoryTest extends \PHPUnit_Framework_TestCase
{
    public function testCreate()
    {
        $factory = new TextFactory();

        $testText = 'text text';
        $testLanguage = $this->createLanguageMock();

        $item = $factory->create($testText, $testLanguage);

        $this->assertEquals($testLanguage, $item->getLanguage());
        $this->assertEquals($testText, $item->getText());
        $this->assertInstanceOf(Text::class, $item);



    }

    public function createLanguageMock() : \PHPUnit_Framework_MockObject_MockObject
    {
        return $this->createMock(LanguageInterface::class);
    }
}