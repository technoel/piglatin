<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $form = $crawler->selectButton('Submit')->form();

        $form['translate[language]']->select('en-GB');

        $csrf = $client->getContainer()->get('security.csrf.token_manager')->getToken('translate')->getValue();

        $crawler = $client->submit($form,[
            'translate[text]'=>'Beast',
            'translate[language]'=>'en-GB',
            'translate[submit]'=>'',
            'translate[_token]'=>$csrf,
        ]);

        $this->assertEquals('east-Bay', $crawler->filterXPath('//pre[@id=\'translation_result\']')->text());

    }
}
